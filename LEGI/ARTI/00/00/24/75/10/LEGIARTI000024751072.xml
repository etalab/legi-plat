<?xml version="1.0" encoding="UTF-8"?>
<ARTICLE>
<META>
<META_COMMUN>
<ID>LEGIARTI000024751072</ID>
<ANCIEN_ID/>
<ORIGINE>LEGI</ORIGINE>
<URL>article/LEGI/ARTI/00/00/24/75/10/LEGIARTI000024751072.xml</URL>
<NATURE>Article</NATURE>
</META_COMMUN>
<META_SPEC>
<META_ARTICLE>
<NUM/>

<ETAT>VIGUEUR</ETAT>

<DATE_DEBUT>2004-07-07</DATE_DEBUT>
<DATE_FIN>2999-01-01</DATE_FIN>


<TYPE>AUTONOME</TYPE>
</META_ARTICLE>
</META_SPEC>
</META>
<CONTEXTE>
<TEXTE autorite="" cid="JORFTEXT000000602781" date_publi="2004-07-06" date_signature="2004-05-21" ministere="MINISTERE DE L'ECONOMIE, DES FINANCES ET DE L'INDUSTRIE" nature="ARRETE" nor="ECOJ0400002A" num="">
<TITRE_TXT c_titre_court="Arrêté du 21 mai 2004" debut="2004-07-07" fin="2999-01-01" id_txt="LEGITEXT000024751060">Arrêté du 21 mai 2004 portant adoption des règles relatives à la comptabilité générale de l'Etat</TITRE_TXT>
<TITRE_TXT c_titre_court="Arrêté du 21 mai 2004" debut="2999-01-01" fin="2999-01-01" id_txt="JORFTEXT000000602781">Arrêté du 21 mai 2004 portant adoption des règles relatives à la comptabilité générale de l'Etat</TITRE_TXT>
<TM>
<TITRE_TM debut="2004-07-07" fin="2999-01-01" id="LEGISCTA000024751066">RECUEIL DES NORMES COMPTABLES DE L’ETAT</TITRE_TM>
</TM>
</TEXTE>
</CONTEXTE>
<VERSIONS>
<VERSION etat="VIGUEUR">
<LIEN_ART debut="2004-07-07" etat="VIGUEUR" fin="2999-01-01" id="LEGIARTI000024751072" num="" origine="LEGI"/>
</VERSION>
</VERSIONS>
<NOTA>
<CONTENU/>
</NOTA>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<p align="center">NORME N°4 - LES PRODUITS DE FONCTIONNEMENT,<br/>LES PRODUITS D'INTERVENTION ET LES PRODUITS FINANCIERS</p>
<p align="center">EXPOSE DES MOTIFS</p>
<p>La présente norme vise à définir les notions de produits de fonctionnement, de produits d'intervention et de produits financiers de l'Etat ainsi qu'à déterminer les règles de comptabilisation et d'évaluation de ces produits conformément aux principes de la comptabilité d'exercice. </p>
<p>I - DEMARCHE RETENUE POUR L'ELABORATION DE LA PRESENTE NORME </p>
<p>Tout en respectant les principes généraux de la comptabilité d'entreprise, la présente norme précise le classement comptable des produits et le critère de rattachement des produits à l'exercice. </p>
<p>I.1 - Le classement comptable des produits </p>
<p>I.1.1 - Les catégories comptables </p>
<p>Les produits relevant de la présente norme ne sont pas des produits spécifiques à l'Etat : il s'agit de produits liés à des ventes de biens ou à des prestations de services, à la détention d'actifs financiers ou à l'utilisation par des tiers d'actifs productifs de redevances, à des subventions reçues de tiers, etc. </p>
<p>Le classement des produits en différentes catégories tient compte des principes suivants : </p>
<p>- la norme ne retient pas les notions de produits exceptionnels et extraordinaires. Comme pour les charges (cf. exposé des motifs de la norme n° 2), l'approche "extraordinaire- ordinaire" plutôt qu'"exceptionnelle-courante" a été retenue. Toutefois, eu égard à l'application des critères IFAC à l'Etat, aucun des produits qui entrent dans le champ d'application de la présente norme ne constitue un produit extraordinaire ; </p>
<p>- la norme identifie des produits d'intervention. Le contenu de ces produits n'est pas spécifique à l'Etat dans la mesure où ils correspondent à des éléments de nature similaire à ceux des entreprises et qui sont, pour l'essentiel, des participations ou des subventions reçues de tiers (Union européenne, collectivités territoriales, partenaires privés) ; </p>
<p>- la norme retient le même périmètre pour les produits financiers que celui des charges financières, à savoir le périmètre des immobilisations financières, de la trésorerie, des dettes financières et des instruments financiers à terme. Par conséquent, les gains de change qui concernent les opérations autres que celles liées au financement et à la trésorerie de l'Etat sont classés conformément à la nature de l'opération à laquelle ils se rapportent, c'est-à-dire dans les produits de fonctionnement. </p>
<p>Ainsi, les produits relevant de la présente norme sont classés en trois catégories comptables : </p>
<p>- les produits de fonctionnement, qui visent l'ensemble des produits se rapportant à l'activité ordinaire de l'Etat ; </p>
<p>- les produits d'intervention, qui correspondent aux produits reçus de tiers sans contrepartie équivalente pour le tiers ; </p>
<p>- les produits financiers, qui sont les produits relatifs aux immobilisations financières, à la trésorerie, aux dettes financières et aux instruments financiers à terme. </p>
<p>I.1.2 - Le classement des fonds de concours et des attributions de produits dans les catégories comptables </p>
<p>L'article 17 de la loi organique autorise des procédures budgétaires particulières qui consistent à affecter une recette à une dépense précise : les fonds de concours et les attributions de produits. </p>
<p>Ces procédures budgétaires ne remettent pas en cause le principe de classement comptable des produits par nature : </p>
<p>- les produits visés par la procédure des fonds de concours sont des produits reçus de tiers pour concourir à des dépenses d'intérêt public ainsi que des produits de legs et de donations attribués à l'Etat. Si les produits relatifs aux dons et legs sont des produits d'intervention, ceux reçus de tiers pour concourir à des dépenses d'intérêt public correspondent, en revanche, soit à des produits de fonctionnement (produits des cessions de biens appartenant à l'Etat par exemple) soit à des produits d'intervention (produits relatifs aux programmes co-financés par exemple) ; </p>
<p>- les produits visés par la procédure des attributions de produits sont réservés aux rémunérations pour services rendus. Il s'agit de produits de fonctionnement. </p>
<p>I.2 - La détermination du critère de rattachement des produits à l'exercice </p>
<p>Pour déterminer le critère de rattachement des produits à l'exercice, la présente norme retient le principe général selon lequel un produit est comptabilisé lorsqu'il est acquis à l'Etat, sous réserve que le produit ou que le résultat de l'opération, dans le cas de contrats à long terme, puisse être mesuré de manière fiable. </p>
<p>La norme décline ce principe par catégorie de produits et distingue les critères de rattachement selon l'activité de l'Etat à savoir l'activité de fonctionnement, l'activité d'intervention et l'activité financière. </p>
<p>II - POSITIONNEMENT DE LA NORME PAR RAPPORT AUX AUTRES REFERENTIELS COMPTABLES </p>
<p>Pour déterminer les règles de comptabilisation et d'évaluation des produits de fonctionnement, des produits d'intervention et des produits financiers, la norme s'inspire des principes généraux du Plan comptable général. </p>
<p>Pour définir son périmètre d'application, la norme s'inspire d'une part, de la norme IAS 18 relative aux "produits des activités ordinaires" et de la norme IPSAS 9 traitant des produits issus d'"opérations avec contrepartie", et d'autre part, des travaux du Comité secteur public de l'IFAC (11) portant sur les produits issus d'"opérations sans contrepartie". </p>
<p>La norme reprend ces deux approches et définit les produits de son champ d'application comme : </p>
<p>- des produits provenant d'opérations ayant une contrepartie directe d'une valeur équivalente pour les tiers (vente de biens ou prestation de services, cession ou utilisation par des tiers d'actifs incorporels, corporels ou financiers, etc.) ; </p>
<p>- ou des produits issus d'opérations sans contrepartie directe équivalente pour les tiers si ces opérations ne sont pas issues de l'exercice de la souveraineté de l'Etat (dons et legs, subventions reçues, etc.) ; </p>
<p>Ainsi, ces produits se distinguent des produits régaliens, qui sont issus de l'exercice de la souveraineté de l'Etat et qui proviennent de tiers qui ne reçoivent pas directement, en contrepartie, une ressource d'une valeur équivalente (impôts, amendes et autres pénalités).</p>
<p>(11) International federation of accountants: fédération internationale des experts- comptables</p>
<p align="center">NORME N°4 - LES PRODUITS DE FONCTIONNEMENT, LES PRODUITS D'INTERVENTION<br/>ET LES PRODUITS FINANCIERS</p>
<p align="center">DISPOSITIONS NORMATIVES</p>
<p>1. CHAMP D'APPLICATION </p>
<p>1.1 CHAMP D'APPLICATION DE LA NORME </p>
<p>La présente norme s'applique aux produits de fonctionnement, aux produits d'intervention et aux produits financiers de l'Etat qui correspondent : </p>
<p>- soit à des opérations ayant une contrepartie directe d'une valeur équivalente pour les tiers (ventes de biens ou prestations de services, cessions ou utilisations par des tiers d'actifs incorporels, corporels ou financiers, etc.) ; </p>
<p>- soit à des opérations sans contrepartie directe équivalente pour les tiers si ces opérations ne sont pas issues de l'exercice de la souveraineté de l'Etat. </p>
<p>En revanche, la présente norme ne s'applique pas aux produits régaliens de l'Etat, c'est-à-dire aux produits issus de l'exercice de la souveraineté de l'Etat et provenant de tiers qui ne reçoivent pas directement, en contrepartie, une ressource d'une valeur équivalente. </p>
<p>1.2 CATEGORIES DE PRODUITS </p>
<p>1.2.1 Les produits de fonctionnement </p>
<p>Les produits de fonctionnement correspondent à l'ensemble des produits issus de l'activité ordinaire de l'Etat. Ils se composent :</p>
<p>- des produits liés aux ventes et aux prestations de services ;</p>
<p>- des produits des cessions d'éléments d'actifs ;</p>
<p>- des autres produits de gestion ordinaire ;</p>
<p>- de la production stockée et immobilisée.</p>
<p>1.2.2 Les produits d'intervention </p>
<p>Les produits d'intervention sont les versements reçus de tiers sans contrepartie équivalente pour le tiers. Ils se composent essentiellement des contributions reçues de tiers. </p>
<p>1.2.3 Les produits financiers </p>
<p>Les produits financiers sont les produits résultant des immobilisations financières, de la trésorerie, des dettes financières, des instruments financiers à terme et des garanties accordées par l'Etat. Sont exclus les gains de change concernant les opérations autres que celles liées au financement et à la trésorerie de l'Etat. </p>
<p>Les produits financiers se composent :</p>
<p>- des produits des participations, des avances et des prêts de l'Etat ;</p>
<p>- des produits des créances non immobilisées ;</p>
<p>- des produits des équivalents de trésorerie correspondant aux plus-values obtenues lors de leur cession ;</p>
<p>- des gains de change liés aux dettes financières, aux instruments financiers à terme et aux éléments constitutifs de la trésorerie ; </p>
<p>- des autres produits financiers liés aux dettes financières, aux instruments financiers à terme, aux éléments constitutifs de la trésorerie et aux garanties accordées par l'Etat. </p>
<p>2. COMPTABILISATION </p>
<p>2.1 LES REGLES GENERALES DE COMPTABILISATION </p>
<p>Les produits sont rattachés à l'exercice au cours duquel ils sont acquis à l'Etat, sous réserve que les produits de l'exercice ou que le résultat de l'opération puissent être mesurés de manière fiable. </p>
<p>Dans le tableau des charges nettes, les produits sont présentés nets des décisions d'apurement qui remettent en cause le bien- fondé de la créance initialement comptabilisée. </p>
<p>2.2 APPLICATION PAR CATEGORIE DE PRODUITS </p>
<p>2.2.1 Les produits de fonctionnement </p>
<p>Pour les biens, le critère de rattachement du produit à l'exercice est la livraison de ces biens. </p>
<p>Pour les prestations de services, le critère de rattachement du produit à l'exercice est la réalisation de ces prestations de services. </p>
<p>Pour les contrats à long terme, lorsque le résultat de ces contrats peut être estimé de manière fiable, les produits associés doivent être comptabilisés en fonction du degré d'avancement de l'exécution du contrat à la date de clôture. </p>
<p>2.2.2 Les produits d'intervention </p>
<p>Pour les produits d'intervention, le critère de rattachement du produit à l'exercice est l'établissement de l'acte constatant le produit acquis au titre de l'exercice. </p>
<p>2.2.3 Les produits financiers </p>
<p>Pour les produits financiers constituant des rémunérations, le critère de rattachement des produits à l'exercice est l'acquisition par l'Etat, prorata temporis, de ces rémunérations. </p>
<p>Pour les produits financiers constituant des primes, est rattachée à l'exercice la quote-part de la prime calculée selon la méthode actuarielle. </p>
<p>Pour les produits financiers constituant des gains, le critère de rattachement est la constatation de ces gains, sauf en matière d'instruments financiers à terme de couverture pour lesquels le critère de rattachement est la constatation des charges enregistrées sur l'élément couvert à partir de la date d'échéance du contrat. </p>
<p>3. INFORMATIONS A FOURNIR DANS L'ANNEXE </p>
<p>La nature et le montant des produits à recevoir, des produits constatés d'avance et des produits à étaler ainsi que le traitement des produits à étaler sont présentés en annexe. </p>
</CONTENU>
</BLOC_TEXTUEL>
<LIENS/>
</ARTICLE>
