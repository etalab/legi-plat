<?xml version="1.0" encoding="UTF-8"?>
<ARTICLE>
<META>
<META_COMMUN>
<ID>LEGIARTI000019985945</ID>
<ANCIEN_ID/>
<ORIGINE>LEGI</ORIGINE>
<URL>article/LEGI/ARTI/00/00/19/98/59/LEGIARTI000019985945.xml</URL>
<NATURE>Article</NATURE>
</META_COMMUN>
<META_SPEC>
<META_ARTICLE>
<NUM>Annexe II : Appendice 2</NUM>

<ETAT>VIGUEUR</ETAT>

<DATE_DEBUT>2006-05-04</DATE_DEBUT>
<DATE_FIN>2999-01-01</DATE_FIN>


<TYPE>AUTONOME</TYPE>
</META_ARTICLE>
</META_SPEC>
</META>
<CONTEXTE>
<TEXTE autorite="" cid="LEGITEXT000006074234" date_publi="2999-01-01" date_signature="2999-01-01" ministere="" nature="CODE" nor="" num="">
<TITRE_TXT c_titre_court="Code de l'aviation civile" debut="1967-04-09" fin="2999-01-01" id_txt="LEGITEXT000006074234">Code de l'aviation civile</TITRE_TXT>
<TM>
<TITRE_TM debut="1992-04-12" fin="2999-01-01" id="LEGISCTA000006108718">Annexes</TITRE_TM>
<TM>
<TITRE_TM debut="1992-04-02" fin="2999-01-01" id="LEGISCTA000019906322">Annexe II à la section I du chapitre Ier du titre III du livre Ier de la troisième partie du code de l'aviation civile (art. D131-1 à D131-10)</TITRE_TM>
<TM>
<TITRE_TM debut="1992-04-02" fin="2999-01-01" id="LEGISCTA000019906323">SERVICES DE LA CIRCULATION AERIENNE</TITRE_TM>
</TM>
</TM>
</TM>
</TEXTE>
</CONTEXTE>
<VERSIONS>
<VERSION etat="MODIFIE">
<LIEN_ART debut="1992-04-02" etat="MODIFIE" fin="1994-12-03" id="LEGIARTI000006843581" num="Annexe II relative aux services de la circulation aérienne" origine="LEGI"/>
</VERSION>
<VERSION etat="MODIFIE">
<LIEN_ART debut="1994-12-03" etat="MODIFIE" fin="2006-05-04" id="LEGIARTI000006843582" num="Annexe II : Appendice B" origine="LEGI"/>
</VERSION>
<VERSION etat="VIGUEUR">
<LIEN_ART debut="2006-05-04" etat="VIGUEUR" fin="2999-01-01" id="LEGIARTI000019985945" num="Annexe II : Appendice 2" origine="LEGI"/>
</VERSION>
</VERSIONS>
<NOTA>
<CONTENU/>
</NOTA>
<BLOC_TEXTUEL>
<CONTENU>
<p align="center">Principes régissant l'établissement et l'identification des points significatifs<br/>(Voir Chapitre 2, section 2.13) </p>
<p>1. ÉTABLISSEMENT DES POINTS SIGNIFICATIFS </p>
<p>1.1 Chaque fois que cela est possible, les points significatifs doivent être établis par rapport à des aides de radionavigation installées au sol, de préférence des aides VHF ou à fréquences plus élevées. </p>
<p>1.2 Lorsqu'il n'existe pas de telles aides de radionavigation installées au sol, des points significatifs sont établis en des emplacements qui peuvent être déterminés par des aides autonomes de bord ou par observation visuelle, lorsque la navigation doit être effectuée par référence visuelle au sol. Des points particuliers peuvent être désignés comme points de transfert de contrôle par accord entre organismes adjacents du contrôle de la circulation aérienne ou entre positions de contrôle intéressés. </p>
<p>2. INDICATIFS DES POINTS SIGNIFICATIFS IDENTIFIÉS PAR L'EMPLACEMENT D'UNE AIDE DE RADIONAVIGATION </p>
<p>2.1 Noms en langage clair pour les points significatifs identifiés par l'emplacement d'une aide de radionavigation </p>
<p>2.1.1 Dans la mesure du possible, les points significatifs sont désignés par référence à un point géographique identifiable et de préférence important. </p>
<p>2.1.2 Dans le choix d'un nom pour le point significatif, on veille à ce que les conditions ci-après soient réunies : </p>
<p>a) le nom ne pose aucune difficulté de prononciation pour les pilotes ou le personnel ATS lorsqu'ils utilisent la langue employée dans les communications ATS. Lorsque le nom d'un emplacement géographique dans la langue nationale choisie pour désigner un point significatif pose des difficultés de prononciation, une forme abrégée ou contractée de ce nom, lui conservant le plus possible sa signification géographique, est choisie; </p>
<p>Exemple : FUERSTENFELDBRUCK = FURSTY</p>
<p>b) le nom est aisément reconnaissable dans les communications en phonie et ne prête pas à confusion avec d'autres points significatifs de la même région d'ensemble. En outre, le nom ne crée pas de confusion par rapport à d'autres communications échangées entre les services de la circulation aérienne et les pilotes ;</p>
<p>c) le nom doit si possible comprendre au moins 6 lettres formant 2 syllabes et, de préférence, un maximum de 3 ;</p>
<p>d) le nom choisi est le même pour le point significatif et pour l'aide de radionavigation dont l'emplacement identifie ce point. </p>
<p>2.2 Composition des indicatifs codés de points significatifs identifiés par l'emplacement d'une aide de radionavigation </p>
<p>2.2.1 L'indicatif codé correspond à l'identification radio de l'aide de radionavigation; il est, si possible, de nature à faciliter le rapprochement avec le nom du point significatif en langage clair. </p>
<p>2.2.2 Le même indicatif codé n'est pas réutilisé à moins de 1 100 km (600 NM) de l'emplacement de l'aide de radionavigation en cause, sauf dans le cas indiqué ci-après. </p>
<p>Note. - Lorsque deux aides de radionavigation fonctionnant dans des bandes différentes du spectre des fréquences sont situées au même emplacement, leur identification radio est en principe la même. </p>
<p>2.3 Les besoins en indicatifs codés sont notifiés aux bureaux régionaux de l'OACI en vue de leur coordination. </p>
<p>3. INDICATIFS DES POINTS SIGNIFICATIFS QUI NE SONT PAS IDENTIFIÉS PAR L'EMPLACEMENT D'UNE AIDE DE RADIONAVIGATION </p>
<p>3.1 Lorsqu'il est nécessaire d'établir un point significatif à un endroit qui n'est pas identifié par l'emplacement d'une aide de radionavigation, ce point significatif est désigné par un groupe nom-indicatif codé unique de 5 lettres qui soit prononçable. Ce nom de code sert alors de nom aussi bien que d'indicatif codé au point significatif. </p>
<p>3.2 Le nom de code est choisi de manière à éviter toute difficulté de prononciation pour les pilotes ou le personnel ATS lorsqu'ils emploient la langue utilisée dans les communications ATS. </p>
<p>Exemples: ADOLA, KODAP</p>
<p>3.3 Le nom de code est facilement identifiable dans les communications en phonie et ne prête pas à confusion avec les indicatifs utilisés pour d'autres points significatifs de la même région d'ensemble. </p>
<p>3.4 Le nom de code assigné à un point significatif n'est pas assigné à un autre point significatif. </p>
<p>3.5 Les besoins en noms de code sont notifiés aux bureaux régionaux de l'OACI en vue de leur coordination </p>
<p>3.6 Dans les régions où il n'existe pas de système de routes fixes ou lorsque les routes suivies par des aéronefs varient en fonction de considérations opérationnelles, les points significatifs sont déterminés et communiqués en coordonnées géographiques du Système géodésique mondial - 1984 (WGS-84); toutefois, les points significatifs établis de manière permanente et servant de points d'entrée ou de points de sortie dans ces régions sont désignés conformément aux dispositions pertinentes des sections 2 ou 3. </p>
<p>4. EMPLOI DES INDICATIFS DANS LES COMMUNICATIONS </p>
<p>4.1 En principe, le nom choisi comme il est indiqué aux sections 2 ou 3 est utilisé pour désigner le point significatif dans les communications en phonie. Si le nom en langage clair d'un point significatif identifié par l'emplacement d'une aide de radionavigation, choisi conformément à la disposition de 2.1, n'est pas utilisé, ce nom est remplacé par l'indicatif codé. Dans les communications en phonie, cet indicatif codé est épelé conformément au code d'épellation de l'OACI. </p>
<p>4.2 Dans les communications imprimées ou codées, seul l'indicatif codé ou le nom de code choisi est utilisé pour désigner un point significatif. </p>
<p>5. POINTS SIGNIFICATIFS UTILISÉS COMME POINTS DE COMPTE RENDU </p>
<p>5.1 Afin de permettre aux services ATS d'obtenir des renseignements concernant la progression des aéronefs en vol, il peut être nécessaire de désigner comme points de compte rendu des points significatifs sélectionnés. </p>
<p>5.2 Pour l'établissement de ces points de compte rendu, on tient compte des facteurs suivants :</p>
<p>a) type des services de la circulation aérienne assurés ;</p>
<p>b) volume de circulation normalement constaté ;</p>
<p>c) précision avec laquelle les aéronefs peuvent se conformer au plan de vol en vigueur ;</p>
<p>d) vitesse des aéronefs ;</p>
<p>e) minimums d'espacement appliqués ;</p>
<p>f) complexité de la structure de l'espace aérien ;</p>
<p>g) méthode(s) de contrôle utilisée(s) ;</p>
<p>h) début ou fin des phases importantes d'un vol (montée, descente, changement de direction, etc.) ;</p>
<p>i) procédures de transfert de contrôle ;</p>
<p>j) sécurité, recherches et sauvetage ;</p>
<p>k) charge de travail dans le poste de pilotage et volume des communications air-sol. </p>
<p>5.3 On attribue aux points de compte rendu l'un des qualificatifs suivants : obligatoires ou sur demande. </p>
<p>On s'inspire des principes suivants pour établir des points de compte rendu obligatoires : </p>
<p>a) le nombre des points de compte rendu obligatoires est limité au minimum qui est nécessaire à la communication régulière de renseignements sur la progression des vols aux organismes des services de la circulation aérienne, compte tenu de la nécessité de réduire au minimum la charge de travail dans le poste de pilotage et celle des contrôleurs, ainsi que le volume des communications air-sol ;</p>
<p>b) le fait qu'une aide de radionavigation soit installée à un emplacement donné ne doit pas déterminer nécessairement sa désignation comme point de compte rendu obligatoire ;</p>
<p>c) des points significatifs obligatoires ne doivent pas nécessairement être établis aux limites d'une région d'information de vol ou d'une région de contrôle. </p>
<p>5.5 Des points de compte rendu sur demande peuvent être établis en fonction des comptes rendus de position additionnels dont les services de la circulation aérienne ont besoin lorsque les conditions de la circulation aérienne l'exigent. </p>
<p>5.6 On réexamine à intervalles réguliers la désignation des points de compte rendu obligatoires et sur demande afin de réduire les comptes rendus réguliers de position au minimum nécessaire pour assurer l'efficacité des services de la circulation aérienne. </p>
<p>5.7 Les comptes rendus réguliers au passage des points de compte rendu obligatoires ne doivent pas être systématiquement obligatoires pour tous les vols et en toutes circonstances. En appliquant ce principe, on prête particulièrement attention aux points suivants : </p>
<p>a) les aéronefs rapides qui évoluent à grande altitude ne doivent pas être tenus de faire des comptes rendus de position réguliers au passage de tous les points qui ont été déclarés de compte rendu obligatoires pour les aéronefs lents évoluant à faible altitude ;</p>
<p>b) les aéronefs qui traversent une région de contrôle terminale ne doivent pas être tenus de faire des comptes rendus réguliers de position aussi souvent que les aéronefs à l'arrivée et au départ. </p>
<p>5.8 Dans les régions où les principes énoncés ci-dessus, pour l'établissement des points de compte rendu, ne peuvent être appliqués, un système de compte rendu défini par rapport aux méridiens ou aux parallèles exprimés en degrés entiers peut être établi. </p>
</CONTENU>
</BLOC_TEXTUEL>
<LIENS>
<LIEN cidtexte="JORFTEXT000000814781" datesignatexte="2006-03-03" id="JORFARTI000001108503" naturetexte="ARRETE" nortexte="EQUA0501901A" num="1" numtexte="" sens="source" typelien="MODIFICATION">Arrêté du 3 mars 2006 - art. 1, v. init.</LIEN>
</LIENS>
</ARTICLE>
